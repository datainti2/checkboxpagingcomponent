import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MdButtonModule, MdCheckboxModule} from '@angular/material';
import { BaseWidgetComponent } from './src/base-widget/base-widget.component';
export * from './src/base-widget/base-widget.component';

@NgModule({
  imports: [
    CommonModule,
    MdButtonModule, 
    MdCheckboxModule
  ],
  declarations: [
	BaseWidgetComponent
  ],
  exports: [
   BaseWidgetComponent
  ]
})
export class CheckboxPagingModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CheckboxPagingModule,
      providers: []
    };
  }
}
